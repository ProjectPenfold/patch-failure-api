import flask
from flask import request, jsonify
import sqlite3

app = flask.Flask(__name__)


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


#API Homepage
@app.route('/', methods=['GET'])
def home():
    return '''<h1>Patch Failure API v1</h1>
<p>Test API for failures logged by patch management software.</p>
<p>Grab all the failures this month:</p>
<p style="margin-left: 40px">/api/v1/resources/failures/all</p>
<p>Filter failures by device, client and/or patch name:</p>
<p style="margin-left: 40px">/api/v1/resources/failures?device=&lt;device-name&gt;</p>
<p style="margin-left: 40px">/api/v1/resources/failures?client=&lt;client-name&gt;</p>
<p style="margin-left: 40px">/api/v1/resources/failures?patch=&lt;patch-name&gt;</p>
<p style="margin-left: 40px">/api/v1/resources/failures?device=&lt;device-name&gt;&amp;client=&lt;client-name&gt;&amp;patch=&lt;patch-name&gt;</p>
<p>Get most frequent failures. Grouped by device, client or patch.</p>
<p style="margin-left: 40px">/api/v1/resources/failures/top?prop=&lt;device/client/patch&gt;</p>'''


#Get all from db
@app.route('/api/v1/resources/failures/all', methods=['GET'])
def api_all():
    conn = sqlite3.connect('failures.db')
    conn.row_factory = dict_factory
    cur = conn.cursor()
    all_failures = cur.execute('SELECT * FROM failures;').fetchall()

    return jsonify(all_failures)


#HTTP Error handling
@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1>", 404


#Filter all failures using the specific properties device, client and patch
@app.route('/api/v1/resources/failures', methods=['GET'])
def api_filter():
    query_parameters = request.args

    device = query_parameters.get('device')
    client = query_parameters.get('client')
    patch = query_parameters.get('patch')

    query = "SELECT * FROM failures WHERE"
    to_filter = []

    #Where no evaluation defaults to if x is true
    if device:
        query += ' device=? AND'
        to_filter.append(device)
    if client:
        query += ' client=? AND'
        to_filter.append(client)
    if patch:
        query += ' patch=? AND'
        to_filter.append(patch)
    if not (device or client or patch):
        return page_not_found(404)

    #Remove the last ' AND' from the query and end with ';'
    query = query[:-4] + ';'

    conn = sqlite3.connect('failures.db')
    conn.row_factory = dict_factory
    cur = conn.cursor()

    results = cur.execute(query, to_filter).fetchall()

    return jsonify(results)


#top of the failures page
@app.route('/api/v1/resources/failures/top', methods=['GET'])
def api_top():
    query_parameters = request.args

    property = query_parameters.get('prop')

    query = 'select ' + property + ', count(' + property + ') FROM failures GROUP BY ' + property + ' ORDER BY count(' + property+ ') DESC;'

    conn = sqlite3.connect('failures.db')
    conn.row_factory = dict_factory
    cur = conn.cursor()
    top_failures = cur.execute(query).fetchall()

    return jsonify(top_failures) 

app.run(host='0.0.0.0',port=5000)
